---
    title: "District Three"
    site: "https://districtthree.be/"
    city: "Lier"
    location: "Kartuizersvest 70, 2500 Lier"
    contact: "info@districtthree.be"
    irc: "http://discord.gg/SfGxJf6"
    aliases: [spaces/DistrictThree.html] #redirect from old site uri
---

District Three is a coworking and makerspace in the center of Lier.

We have a wide variety of machines at our disposal: lasercutter, 3D printers, UV printer, XY plotter, Spot welder, CNC Machines, Sublimation printer, button maker and much more.
A detailed list can be found here: https://districtthree.be/machines.html

Every month there are free coderdojo sessions where children can learn how to program or use all kinds of technology.
More information about these open source workshops can be found here: http://lier.coderdojobelgium.be/
These sessions have been going strong since 2014!

Everyone is welcome to work on whatever project they wish. Open sharing of ideas and techniques is highly encouraged: so come and learn with and from us!

* Location: [Kartuizersvest 70, 2500 Lier](https://www.google.com/maps/place/District+Three/@51.1342595,4.5676028,17z/data=!3m1!4b1!4m5!3m4!1s0x47c3fdc1b5be7675:0x112a64f812611fae!8m2!3d51.1342562!4d4.5697915)
* Site: https://districtthree.be/
* Discord: http://discord.gg/SfGxJf6
* Wiki: https://wiki.districtthree.dev/nl/home
* Gitlab: https://git.districtthree.be/groups/district-three
* Issues: https://git.districtthree.be/groups/district-three/-/issues